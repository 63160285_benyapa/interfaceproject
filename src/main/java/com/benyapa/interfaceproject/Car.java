/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.interfaceproject;

/**
 *
 * @author bwstx
 */
public class Car extends Vahicle implements Runable {

    public Car(String engine) {
        super(engine);
    }

    @Override
    public void startEngine() {
        System.out.println("Car : Start Engine");
    }

    @Override
    public void stopEngine() {
        System.out.println("Car : Stop Engine");
    }

    @Override
    public void raiseEngine() {
        System.out.println("Car : Raise Engine");
    }

    @Override
    public void applyBreak() {
        System.out.println("Car : Apply Break");
    }

    @Override
    public void run() {
        System.out.println("Car : Run");
    }

}
