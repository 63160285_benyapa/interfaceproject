/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.interfaceproject;

/**
 *
 * @author bwstx
 */
public abstract class Animal {

    private String name;
    private int numOfLeg;

    public Animal(String name, int numOfLeg) {
        this.name = name;
        this.numOfLeg = numOfLeg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumOfLeg() {
        return numOfLeg;
    }

    public void setNumOfLeg() {
        this.numOfLeg = numOfLeg;
    }

    public abstract void eat();

    public abstract void speak();

    public abstract void sleep();

}
