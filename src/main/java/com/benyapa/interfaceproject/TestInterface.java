/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.interfaceproject;

/**
 *
 * @author bwstx
 */
public class TestInterface {

    public static void main(String[] args) {
        Bat bat = new Bat();
        Plane plane = new Plane("Engine No.1");
        Dog dog = new Dog("Dang",4);
        Car car = new Car("Engine No.2");
        bat.fly();
        plane.fly();
        dog.run();
        car.run();
        System.out.println("----------");

        Flyable[] flyable = {bat, plane};
        for (Flyable f : flyable) {
            if (f instanceof Plane) {
                Plane p = (Plane) f;
                p.startEngine();
                p.run();
            }
            f.fly();
        }
        
        Runable[] runables = {dog,car};
        for(Runable r : runables){
            if(r instanceof Car){
                Car c = (Car)r;
                c.startEngine();
            }
            r.run();
        }
        


    }

}
